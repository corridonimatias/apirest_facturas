const mongoose = require("mongoose");
const express = require('express');
var FacturasRouter = require('./routes/facturas.js');
var ClientesRouter = require('./routes/clientes.js');
const app = express();
const port = 3000;

app.use('/api/facturas', FacturasRouter);
app.use('/api/clientes', ClientesRouter);

app.use(express.static('public'));

mongoose.connect('mongodb://localhost/finanzas');
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
    app.listen(port, () => console.log(`Corriendo en ${port}!`));
});

