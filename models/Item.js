const mongoose = require("mongoose");

const Item = new mongoose.Schema({
    cantidad: { type: Number , min: 0 },
    precio: { type: Number , min: 0 },
    producto: { type:String, trim:true }
});

module.exports = mongoose.model("Item", Item);