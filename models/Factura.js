const mongoose = require("mongoose");
const Cliente = require("./Cliente.js").schema;
const Item = require("./Item.js").schema;

//https://mongoosejs.com/docs/schematypes.html <-- tipos para schema con sus restricciones

const Factura = new mongoose.Schema({
    nroFactura: { type: Number, min: 0 },
    condPago: { type:String, trim:true },
    fechaEmision: Date,     //OJO al guardar dates: https://mongoosejs.com/docs/schematypes.html#dates
    fechaVencimiento: Date,
    cliente: Cliente,
    item: {type:[Item], default: []}
});

Factura.methods.sumar = function () {
    const reducer = (accumulator, item) => accumulator + item.precio;

    return this.item.reduce(reducer, 0);
};


Factura.statics.findByNroFactura = function (nroFactura) {
    return this.findOne({nroFactura: nroFactura});
};

module.exports = mongoose.model("Factura", Factura);